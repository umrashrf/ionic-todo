# TO-DO app made in Ionic v2

Demo URL: http://138.197.159.94/www/

## Screenshots

|![HomePage](https://i.gyazo.com/36f27ab2faabf5db441c4312e761b836.png)|
| -------------------|
| Figure 1: HomePage |

|![Add New Task Page](https://i.gyazo.com/0290cc05b52c302755e1fd8aacc8ba8f.png)|
| -------------------|
|Figure 2: Add New Task Page

|![Details Page 1](https://i.gyazo.com/a6b80fa1f68d42452aa0c50cc9601caa.png)![Details Page 2](https://i.gyazo.com/6f7d8966582c9a9b6b47f5a92e7b6d29.png)|
| -------------------|
|Figure 2: Details Page(s)|