import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { Camera, Transfer } from 'ionic-native';

import { NotesService } from '../../providers/notes';

/*
  Generated class for the Details page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
  providers: [NotesService]
})
export class DetailsPage {

  item: any;
  notes: any;
  note = {};

  start = 0;
  count = 10;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl: LoadingController, public notesService: NotesService) {
    this.item = this.navParams.get('item');
    this.notes = [];
    this.loadNotes();
  }

  loadNotes() {
    this.notesService.load(this.item.id, this.start, this.count)
                      .then(
                        data => this.notes = data
                      );
  }

  loadPrev() {
    this.count += 10; // fixme
    this.notesService.load(this.item.id, this.start, this.count)
                      .then(
                        data => this.notes = data
                      );
  }

  addNote() {
    var _me = this;

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.note['task_id'] = this.item.id;

    this.notesService.add(this.note)
                      .then(function() {
                        loading.dismiss();
                        _me.loadNotes();
                        _me.note = {};
                      });
  }

  takePhoto() {
    var _me = this;

    Camera.getPicture().then((imageData) => {

      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();

      if (imageData.startsWith('file')) {

        const fileTransfer = new Transfer();

        var win = function (r) {
          loading.dismiss();
          _me.loadNotes();
          _me.note = {};
        }

        var fail = function (error) {
          loading.dismiss();
        }

        var options: any;

        options = {
          fileKey: "img",
          fileName: imageData.substr(imageData.lastIndexOf('/') + 1),
          params: {
            task_id: this.item.id
          },
          headers: {}
        }

        fileTransfer.upload(imageData, encodeURI("http://138.197.159.94/index.php/notes"), options).then(win, fail);

      } else {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        alert('Image is not a file. This functionality is not supported yet.');
      }
    }, (err) => {

    });
  }
}
