import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { NewtaskPage } from '../newtask/newtask';
import { DetailsPage } from '../details/details';
import { TasksService } from '../../providers/tasks';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [TasksService]
})
export class HomePage {
  detailsPage = DetailsPage;
  newTaskPage = NewtaskPage;

  items: any;

  constructor(public navCtrl: NavController, public taskService: TasksService) {
    this.items = [];
    this.taskService.load()
                    .then(
                      data => this.items = data
                    );
  }

  gotoNewTaskPage() {
    this.navCtrl.push(this.newTaskPage, {
      homepage: this
    });
  }

  itemSelected(item: string) {
    this.navCtrl.push(this.detailsPage, {
      item: item
    });
  }

  doRefresh() {
    var _me = this;
    this.taskService.load()
                    .then(
                      function(data) {
                        _me.items = data;
                      }
                    );
  }
}
