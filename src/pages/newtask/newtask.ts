import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TasksService } from '../../providers/tasks';

/*
  Generated class for the Newtask page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-newtask',
  templateUrl: 'newtask.html',
  providers: [TasksService]
})
export class NewtaskPage {
  homePage: HomePage;

  item = {}

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl: LoadingController, public taskService: TasksService) {
    this.homePage = this.navParams.get('homepage');
  }

  addTask() {
    var _me = this;

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.taskService.add(this.item)
                    .then(
                      function() {
                        loading.dismiss();
                        _me.homePage.doRefresh();
                        _me.navCtrl.pop();
                      }
                    );
  }
}
