import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Notes provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotesService {

  data: any;
  url = "http://138.197.159.94/index.php/notes";

  constructor(public http: Http) {
    this.data = null;
  }

  load(task_id, start, count) {
    let params = '';
    params += '?task_id=' + encodeURI(task_id);
    params += '&start=' + encodeURI(start);
    params += '&count=' + encodeURI(count);

    return new Promise(resolve => {
      this.http.get(this.url + params)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  add(note) {
    return new Promise(resolve => {
      this.http.post(this.url, note)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

}
